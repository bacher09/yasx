#!/usr/bin/env python
import os.path
try:
    from setuptools import setup
    kwargs = {
        'tests_require': ["nose>=1.0"],
        'test_suite': 'nose.collector'
    }
except ImportError:
    from distutils.core import setup


ROOT_PATH = os.path.dirname(__file__)
with open(os.path.join(ROOT_PATH, "README.rst")) as f:
    long_description = f.read()


with open(os.path.join(ROOT_PATH, "yasx", "__init__.py")) as f:
    import re
    VERSION = re.compile(r'.*__version__ = "(.*?)"', re.S). \
        match(f.read()).group(1)


setup(
    name='Yasx',
    description='Stream XML generator',
    long_description=long_description,
    url="https://bitbucket.org/bacher09/yasx/",
    version=VERSION,
    author='Slava Bacherikov',
    author_email='slava@bacherikov.org.ua',
    packages=["yasx"],
    install_requires=[
        "six",
    ],
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.2",
        "Programming Language :: Python :: 3.3",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    keywords=["xml", "sitemap", "sphinxsearch"],
    license="BSD",
    **kwargs
)
