import datetime
import calendar
import time
from xml.sax.saxutils import escape, quoteattr


def datetime_to_unix(d):
    return int(time.mktime(d.timetuple()))

def datetimeutc_to_unic(d):
    return int(calendar.timegm(d.timetuple()))


def to_timestamp(d):
    if isinstance(d, (datetime.datetime, datetime.date)):
        return datetime_to_unix(d)
    elif isinstance(d, (int, float)):
        return int(d)
    else:
        raise ValueError("Bad datetime")


def format_iso8601(dtime):
    # TODO: Support non UTC datetime with timezones
    if isinstance(dtime, datetime.datetime):
        return dtime.strftime('%Y-%m-%dT%H:%M:%SZ')
    elif isinstance(dtime, datetime.date):
        return dtime.isoformat()
