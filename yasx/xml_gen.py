from .utils import escape, quoteattr
import six


class XmlGenerator(object):

    def start_document(self, encoding="UTF-8"):
        return six.u('<?xml version="1.0" encoding="%s"?>\n') % encoding

    def end_document(self):
        return ""

    def start_tag(self, tag, attrs=None):
        if attrs:
            str_list = []
            format_str = six.u("%s=%s")
            for name, value in six.iteritems(attrs):
                str_list.append(format_str % (name, quoteattr(value)))
            attr_str = " " + six.u(" ").join(str_list)
        else:
            attr_str = ""
        return six.u("").join(["<" + tag, attr_str, ">"])

    def quick_tag(self, tag, data=None, attrs=None):
        return six.u("").join([
            self.start_tag(tag, attrs), 
            self.data(data) if data else "",
            self.end_tag(tag)
        ])

    def end_tag(self, tag):
        return six.u("</%s>") % tag

    def data(self, data):
        return escape(six.u(data))

    def processing_instruction(self, target, data):
        return six.u('<?%s %s?>' % (target, data))


XML = XmlGenerator()
