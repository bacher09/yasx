from .xml_gen import XML
import six


def yield_from(iterator):
    for item in iterator:
        if item is None:
            pass
        elif isinstance(item, six.string_types) or not \
             hasattr(item, "__iter__"):
            yield item
        elif hasattr(item, "__iter__"):
            for subitem in yield_from(item):
                yield subitem


def iter_write(fp, iterator):
    for item in iterator:
        fp.write(item)


class BaseXMLCreator(object):

    xml_generator_factory = staticmethod(lambda: XML)

    def __init__(self):
        self.xml = self.xml_generator_factory()

    def generate_content(self):
        raise NotImplementedError

    def start(self):
        return self.xml.start_document()

    def end(self):
        return self.xml.end_document()

    def generate(self):
        for item in yield_from(self.start()): yield item
        for item in yield_from(self.generate_content()):
            yield item
        for item in yield_from(self.end()): yield item

    def to_file(self, fp):
        iter_write(fp, self.generate())


class BaseXMLItem(object):

    def __init__(self, *args, **kwargs):
        pass

    def yield_xml(self, xml_obj):
        raise NotImplementedError


class IterXMLCreator(BaseXMLCreator):

    xml_item_factory = BaseXMLItem

    def __init__(self, iterator):
        super(IterXMLCreator, self).__init__()
        self.iterator = iterator

    def generate_content(self):
        for item in self.iterator:
            if hasattr(item, "yield_xml"):
                xml_item = item
            elif isinstance(item, dict):
                xml_item = self.xml_item_factory(**item)
            elif isinstance(item, (list, tuple)):
                xml_item = self.xml_item_factory(*item)
            else:
                raise ValueError("Bad item")

            yield xml_item.yield_xml(self.xml)
