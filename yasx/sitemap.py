from .base import BaseXMLItem, IterXMLCreator
from .utils import format_iso8601


class SitemapIndexItem(BaseXMLItem):
    
    def __init__(self, loc, lastmod=None):
        self.loc = loc
        self.lastmod = lastmod

    def yield_xml(self, xml):
        yield xml.start_tag("sitemap")
        yield xml.quick_tag("loc", self.loc)
        if self.lastmod:
            yield xml.quick_tag("lastmod", format_iso8601(self.lastmod))
        yield xml.end_tag("sitemap")


class SitemapIndexCreator(IterXMLCreator):

    xml_item_factory = SitemapIndexItem

    def start(self):
        yield super(SitemapIndexCreator, self).start(),
        # Namespace ?
        yield self.xml.start_tag("sitemapindex", {
                "xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9"
        })

    def end(self):
        yield self.xml.end_tag("sitemapindex"),
        yield super(SitemapIndexCreator, self).end()


class SitemapXMLItem(BaseXMLItem):

    changefreq_set = frozenset([
        "always", "hourly", "daily", "weekly", "monthly", "yearly", "never"
    ])
    
    def __init__(self, loc, lastmod=None, changefreq=None, priority=None):
        self.loc = loc
        self.lastmod = lastmod
        if not (changefreq is None or changefreq in self.changefreq_set):
            raise ValueError("Bad changefreq value '%s'" % changefreq)
        self.changefreq = changefreq
        self.priority = priority

    def yield_xml(self, xml):
        yield xml.start_tag("url")
        yield xml.quick_tag("loc", self.loc)
        if self.lastmod:
            yield xml.quick_tag("lastmod", format_iso8601(self.lastmod))

        if self.changefreq:
            yield xml.quick_tag("changefreq", self.changefreq)

        if self.priority:
            yield xml.quick_tag("priority", str(self.priority))
        yield xml.end_tag("url")


class SitemapXMLCreator(IterXMLCreator):

    xml_item_factory = SitemapXMLItem

    def start(self):
        yield super(SitemapXMLCreator, self).start()
        yield self.xml.start_tag("urlset", {
            "xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9",
        })

    def end(self):
        yield self.xml.end_tag("urlset")
        yield super(SitemapXMLCreator, self).end()
