from .base import BaseXMLItem, IterXMLCreator
from .utils import to_timestamp


class SphinxXMLPipeItem(BaseXMLItem):

    def __init__(self, id, group, title, body, timestamp=None):
        self.id = id
        self.group = group
        self.title = title
        self.body = body
        self.timestamp = timestamp

    def yield_xml(self, xml):
        yield xml.start_tag('document')
        yield xml.quick_tag('id', str(self.id))
        yield xml.quick_tag('group', str(self.group))
        if self.timestamp:
            yield xml.quick_tag('timestamp', str(to_timestamp(self.timestamp)))

        yield xml.quick_tag('title', self.title)
        yield xml.quick_tag('body', self.body)
        yield xml.end_tag('document')


class SphinxXMLPipeCreator(IterXMLCreator):

    xml_item_factory = SphinxXMLPipeItem

    def start(self):
        return ""
