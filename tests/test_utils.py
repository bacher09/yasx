from . import utils
from yasx import utils as yasx_utils
from datetime import datetime


class TestUtils(utils.TestCase):
    
    def test_xml_cmp(self):
        xml1 = """
<root>
    <elem>data</elem>
    <elem attr="val">data</elem>
    <elem attr="val" another-attr="dat">test</elem>
    <test one="1" two="2">new test</test>
</root>"""
        xml2 = """
<root>
    <elem>data</elem>
    <elem attr="val">data</elem>
    <elem attr="val" another-attr="dat">test</elem>
    <test two="2" one="1">new test</test>
</root>"""
        self.assertXMLEqual(xml1, xml1)
        self.assertXMLEqual(xml1, xml2)
