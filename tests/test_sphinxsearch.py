from . import utils
import yasx


class TestSphinx(utils.TestCase):

    def test_simple_xmlpipe1(self):
        res = """
<document>
  <id>1</id>
  <group>1</group>
  <title>First</title>
  <body>Message</body>
</document>
<document>
  <id>2</id>
  <group>1</group>
  <title>Second</title>
  <body>Seccond Message</body>
</document>
<document>
  <id>3</id>
  <group>1</group>
  <timestamp>1382031995</timestamp>
  <title>Third</title>
  <body>Third message</body>
</document>"""
        gen_data = (
            {"id": 1, "group": 1, "title": "First", "body": "Message"},
            {
                "id": 2, "group": 1, "title": "Second",
                "body": "Seccond Message"
            },
            {
                "id": 3, "group": 1, "title": "Third", "body": "Third message",
                "timestamp": 1382031995
            }
        )
        s = self.string_fp()
        # Create generation object
        yasx.SphinxXMLPipeCreator(gen_data).to_file(s)
        self.assertXMLMultipleEqual(s.getvalue(), res,
                                    "Bad result for Sphinx XMLPipe1")
