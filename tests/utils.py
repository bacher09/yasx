import xml.etree.ElementTree as ET
from collections import deque
import unittest
import sys
try:
    from future_builtins import zip
except ImportError:
    try:
        from itertools import izip as zip
    except ImportError:
        pass


PY2 = sys.version_info[0] == 2
PY3 = sys.version_info[0] == 3


if PY3:
    from io import StringIO
else:
    try:
        from cStringIO import StringIO
    except ImportError:
        from StringIO import StringIO


class XMLError(Exception):
    pass


class TreeElement(object):

    def __init__(self, name, attr=None):
        self.name = name
        if attr is not None:
            self.attr = tuple(sorted(attr.items()))
        else:
            self.attr = None

        self.items = []

    def append(self, value):
        if isinstance(value, str):
            if not value.strip():
                return
        self.items.append(value)

    def __repr__(self):
        return "<%(class)s %(tag)s>" % {
            "class": self.__class__.__name__,
            "tag": self.name
        }

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        if self.name == other.name and self.attr == other.attr \
           and len(self.items) == len(other.items):
            for item1, item2 in zip(self.items, other.items):
                if item1 != item2:
                    return False
            return True

        return False

    def __ne__(self, other):
        return not self == other


class ComparableTreeBuilder(object):

    def __init__(self):
        self.root = TreeElement(None)
        self.current = self.root
        self.elem_stack = deque([self.root])

    def start(self, tag, attr):
        elem = TreeElement(tag, attr)
        self.current.append(elem)
        self.elem_stack.append(self.current)
        self.current = elem

    def end(self, tag):
        try:
            parent = self.elem_stack.pop()
        except IndexError:
            raise XMLError("Bad xml data")
        else:
            self.current = parent

    def data(self, data):
        self.current.append(data)

    def close(self):
        if len(self.elem_stack) != 1:
            raise XMLError("Unclosed XML tags")
        return self.root


def parse(data):
    target = ComparableTreeBuilder()
    parser = ET.XMLParser(target=target)
    parser.feed(data)
    return parser.close()


class TestCase(unittest.TestCase):

    string_fp = StringIO

    def assertXMLEqual(self, a, b, msg=None):
        try:
            a_xml = parse(a)
            b_xml = parse(b)
        except XMLError:
            self.fail("Bad xml input")

        if a_xml != b_xml:
            self.fail(msg)

    def assertXMLMultipleEqual(self, a, b, msg=None):
        decor = lambda x: "<root>%s</root>" % x.strip()
        self.assertXMLEqual(decor(a), decor(b), msg=msg)
