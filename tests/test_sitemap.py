from . import utils
import datetime
import yasx


class TestSitemap(utils.TestCase):

    def test_sitemap_index(self):
        res = """\
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <sitemap>
      <loc>http://www.example.com/sitemap1.xml.gz</loc>
      <lastmod>2004-10-01</lastmod>
   </sitemap>
   <sitemap>
      <loc>http://www.example.com/sitemap2.xml.gz</loc>
      <lastmod>2005-01-01</lastmod>
   </sitemap>
   <sitemap>
      <loc>http://www.example.com/sitemap3.xml.gz</loc>
   </sitemap>
</sitemapindex>"""

        gen_data = (
            {
                "loc": "http://www.example.com/sitemap1.xml.gz",
                "lastmod": datetime.date(2004, 10, 1)
            },
            {
                "loc": "http://www.example.com/sitemap2.xml.gz",
                "lastmod": datetime.date(2005, 1, 1)
            },
            {"loc": "http://www.example.com/sitemap3.xml.gz"},
        )
        s = self.string_fp()
        yasx.SitemapIndexCreator(gen_data).to_file(s)
        self.assertXMLEqual(s.getvalue(), res,
                                    "Bad result for Sitemap Index")

    def test_sitemap(self):
        res = """\
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <url>
      <loc>http://www.example.com/</loc>
      <lastmod>2005-01-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
   </url>
   <url>
      <loc>http://www.example.com/test/</loc>
      <lastmod>2006-06-01</lastmod>
      <changefreq>daily</changefreq>
      <priority>0.9</priority>
   </url>
</urlset>"""

        gen_data = (
            {
                "loc": "http://www.example.com/",
                "lastmod": datetime.date(2005, 1, 1),
                "changefreq": "monthly",
                "priority": 0.8
            },
            {
                "loc": "http://www.example.com/test/",
                "lastmod": datetime.date(2006, 6, 1),
                "changefreq": "daily",
                "priority": 0.9
            },
        )
        s = self.string_fp()
        yasx.SitemapXMLCreator(gen_data).to_file(s)
        self.assertXMLEqual(s.getvalue(), res,
                                    "Bad result for Sitemap")
